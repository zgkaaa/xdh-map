# Scale 比例尺

::: tip 提示
 显示地图比例尺， 组件[API文档](/api.html?url=/xdh-map/doc/module-xdh-map-scale.html)
:::

## 基础用法

:::demo

```html
<template>
    <xdh-map>
      <xdh-map-scale units="metric"></xdh-map-scale>
    </xdh-map>
</template>

<script>
 import {XdhMap, XdhMapScale} from 'xdh-map'
  
  export default {
    components: {
     XdhMap,
     XdhMapScale
   }
  }
</script>
```

:::