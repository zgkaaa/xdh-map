import Pointer from './src/pointer'

Pointer.install = function (Vue) {
  Vue.component(Pointer.name, Pointer)
}
export default Pointer
