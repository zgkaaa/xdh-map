import Placement from './src/placement'

Placement.install = function (Vue) {
  Vue.component(Placement.name, Placement)
}
export default Placement
